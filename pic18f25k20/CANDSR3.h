#include <18F25K80.h>
#device ADC=16
#device *=16

#FUSES WDT                      //Watch Dog Timer
#FUSES WDT512                   //Watch Dog Timer uses 1:512 Postscale
#FUSES NOXINST                  //Extended set extension and Indexed Addressing mode disabled (Legacy mode)
#FUSES PUT                      //Power Up Timer
#FUSES NOBROWNOUT               //No brownout reset

#use delay(crystal=20000000)
#use rtos(timer=0, minor_cycle=1 ms)

#define SNCANDeviceType 0x000F
#define SNCANMemoryBlocks 85
#define SNCANio_count 153

#define Logic_ch2  PIN_B6
#define Logic_ch1  PIN_B7

#define LED  PIN_B5
#define POWER1  PIN_C5
#define POWER2  PIN_C4
#define TOGGLE1  PIN_C2
#define TOGGLE2  PIN_C3
#define ZUMMER  PIN_B4
#define IR  PIN_B0

//memory config
#define Nighttime_default 43200; //night status max time
#define Daytime_default 65535; //night status max time
#define Toggle_switch_time 10


