#include <main.h>
#include "dui.c"

unsigned int8 adc_vol = 0;

//TASK MESSAGE CHECK
#task(rate = 10 ms, max = 20 us)
void adc_read_processed_task();

#task(rate = 100 ms, max = 30 us)
void task_DUIsend();

#task(rate = 1 ms, max = 300 us)
void task_DUIcheck();

//======================== ADC MODULE ==============================
void adc_read_processed_task() {
        adc_vol = read_adc(ADC_READ_ONLY);
        read_adc(ADC_START_ONLY); 
}

//-------------------------------------------------
void task_DUIsend() {
   DUISendUInt16(0, adc_vol); 
}

void task_DUIcheck() {
   DUICheck();
}

void formInit(void) {
   char text1[]="vol";
   char text2[]="";
   DUIAddChart(0, text1, text2);
}

//implementation
void initialization() {
    SET_TRIS_A( 0x01 );
    SET_TRIS_B( 0x01 );
    SET_TRIS_C( 0x84 );
    DUIInit();
    DUIOnFormInit = formInit;

    setup_adc(ADC_CLOCK_DIV_64|ADC_TAD_MUL_0);
    setup_adc_ports(sAN0);
    set_adc_channel(0);
    read_adc(ADC_START_ONLY);
}

void main()
{
   initialization();
   rtos_run();
}
